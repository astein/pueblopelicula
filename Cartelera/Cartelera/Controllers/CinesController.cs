﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Cartelera.DAL;
using Cartelera.Filters;
using Newtonsoft.Json;

namespace Cartelera.Controllers
{
    public class CinesController : Controller
    {
        private CarteleraContext db = new CarteleraContext();

        // GET: Cines
        [AdminValidator]
        public ActionResult Index()
        {
            ViewBag.Cines = new SelectList(db.Cines, "Id", "Nombre");
            
            return View();
        }
        
        public String GetSalasByCine(int id)
        {
            return JsonConvert.SerializeObject(db.Salas.Where(s => s.CineId == id).Select(s => new
            {
                Id = s.Id,
                CineId = s.CineId,
            }).ToList(),
                Formatting.None,
                new JsonSerializerSettings()
                {
                    ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                });
        }

        public String GetFuncionesBySala(int id)
        {
            return JsonConvert.SerializeObject(db.Funciones.Where(f => f.SalaId == id).Select(f => new
                {
                    Id = f.Id,
                    Horario = f.Horario,
                    Pelicula = f.Pelicula,
                }).ToList(),
                Formatting.None,
                new JsonSerializerSettings()
                {
                    ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                });
        }
    }
    
}
