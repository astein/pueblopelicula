﻿using System;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Cartelera.DAL;
using Cartelera.Filters;
using Cartelera.Models;
using Cartelera.Models.Peliculas;

namespace Cartelera.Controllers
{
    public class ComercialesController : Controller
    {
        private CarteleraContext db = new CarteleraContext();

        // GET: Comerciales
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Peliculas", db.Peliculas.ToList());
        }

        // GET: Comerciales/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Comercial comercial = (Comercial)db.Peliculas.Find(id);
            if (comercial == null)
            {
                return HttpNotFound();
            }

            ViewBag.Funciones = db.Funciones.Where(f => f.PeliculaId == comercial.Id).ToList().OrderBy(f => f.Horario);
            ViewBag.Cines = db.Cines.ToList();

            return View(comercial);
        }

        [AdminValidator]
        // GET: Comerciales/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Comerciales/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AdminValidator]
        public ActionResult Create([Bind(Include = "Id,Titulo,Duracion,Archivo,Foto,Sinopsis,Actores")] Comercial comercial)
        {
            var fileStream = comercial.Archivo.InputStream;
            var binaryReader = new BinaryReader(fileStream);
            var bytes = binaryReader.ReadBytes((Int32)fileStream.Length);
            var base64String = Convert.ToBase64String(bytes, 0, bytes.Length);

            comercial.Foto = base64String;
            if (ModelState.IsValid)
            {
                db.Peliculas.Add(comercial);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(comercial);
        }

        // GET: Comerciales/Edit/5

        [AdminValidator]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Comercial comercial = (Comercial)db.Peliculas.Find(id);
            if (comercial == null)
            {
                return HttpNotFound();
            }
            return View(comercial);
        }

        // POST: Comerciales/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AdminValidator]
        public ActionResult Edit([Bind(Include = "Id,Titulo,Duracion,Foto,Sinopsis,Actores")] Comercial comercial)
        {
            if (ModelState.IsValid)
            {
                db.Entry(comercial).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(comercial);
        }

        // GET: Comerciales/Delete/5

        [AdminValidator]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Comercial comercial = (Comercial)db.Peliculas.Find(id);
            if (comercial == null)
            {
                return HttpNotFound();
            }
            return View(comercial);
        }

        // POST: Comerciales/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [AdminValidator]
        public ActionResult DeleteConfirmed(int id)
        {
            Comercial comercial = (Comercial)db.Peliculas.Find(id);
            foreach (Cine cine in db.Cines.ToList())
            {
                if (cine.Contiene(comercial))
                {
                    return Content("<script language='javascript' type='text/javascript'>alert('No se permite eliminar peliculas en cartelera');</script>");

                }
            }
            db.Peliculas.Remove(comercial);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
