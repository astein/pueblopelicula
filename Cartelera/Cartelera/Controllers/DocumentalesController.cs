﻿using System;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Cartelera.DAL;
using Cartelera.Filters;
using Cartelera.Models;
using Cartelera.Models.Peliculas;

namespace Cartelera.Controllers
{
    public class DocumentalesController : Controller
    {
        private CarteleraContext db = new CarteleraContext();

        // GET: Documentales
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Peliculas", db.Peliculas.ToList());
        }

        // GET: Documentales/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Documental documental = (Documental)db.Peliculas.Find(id);
            if (documental == null)
            {
                return HttpNotFound();
            }

            ViewBag.Funciones = db.Funciones.Where(f => f.PeliculaId == documental.Id).ToList().OrderBy(f => f.Horario);
            ViewBag.Cines = db.Cines.ToList();

            documental.Tema = db.Temas.Find(documental.TemaId);
            return View(documental);
        }

        // GET: Documentales/Create

        [AdminValidator]
        public ActionResult Create()
        {
            ViewBag.TemaId = new SelectList(db.Temas, "Id", "Descripcion");
            return View();
        }

        // POST: Documentales/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [AdminValidator]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Titulo,Duracion,Archivo,Foto,Sinopsis,TemaId")] Documental documental)
        {
            var fileStream = documental.Archivo.InputStream;
            var binaryReader = new BinaryReader(fileStream);
            var bytes = binaryReader.ReadBytes((Int32)fileStream.Length);
            var base64String = Convert.ToBase64String(bytes, 0, bytes.Length);

            documental.Foto = base64String;
            if (ModelState.IsValid)
            {
                db.Peliculas.Add(documental);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.TemaId = new SelectList(db.Temas, "Id", "Descripcion", documental.TemaId);
            return View(documental);
        }

        [AdminValidator]
        // GET: Documentales/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Documental documental = (Documental)db.Peliculas.Find(id);
            if (documental == null)
            {
                return HttpNotFound();
            }
            ViewBag.TemaId = new SelectList(db.Temas, "Id", "Descripcion", documental.TemaId);
            return View(documental);
        }

        // POST: Documentales/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AdminValidator]
        public ActionResult Edit([Bind(Include = "Id,Titulo,Duracion,Foto,Sinopsis,TemaId")] Documental documental)
        {
            if (ModelState.IsValid)
            {
                db.Entry(documental).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.TemaId = new SelectList(db.Temas, "Id", "Descripcion", documental.TemaId);
            return View(documental);
        }

        [AdminValidator]
        // GET: Documentales/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Documental documental = (Documental)db.Peliculas.Find(id);
            if (documental == null)
            {
                return HttpNotFound();
            }
            return View(documental);
        }

        // POST: Documentales/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [AdminValidator]
        public ActionResult DeleteConfirmed(int id)
        {
            Documental documental = (Documental)db.Peliculas.Find(id);
            foreach (Cine cine in db.Cines.ToList())
            {
                if (cine.Contiene(documental))
                {
                    return Content("<script language='javascript' type='text/javascript'>alert('No se permite eliminar peliculas en cartelera');</script>");
                }
            }
            db.Peliculas.Remove(documental);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
