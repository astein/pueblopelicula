﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web.Mvc;
using Cartelera.DAL;
using Cartelera.Filters;
using Cartelera.Models;
using Cartelera.Models.Peliculas;
using Cartelera.Models.Salas;

namespace Cartelera.Controllers
{
    public class FuncionesController : Controller
    {
        //TODO Podria fusionarse con Cines

        private CarteleraContext db = new CarteleraContext();

        // GET: Funciones/Create/5
        [AdminValidator]
        public ActionResult Create(int id)
        {
            Sala sala = db.Salas.Find(id);
            
            if (sala !=null && ObjectContext.GetObjectType(sala.GetType()) == typeof(SalaHd))
            {
                List<Pelicula> peliculas = new List<Pelicula>();
                foreach (Pelicula pelicula in db.Peliculas.ToList().OrderBy(p=>p.Titulo))
                {
                    if (ObjectContext.GetObjectType(pelicula.GetType()) == typeof(Comercial))
                    {
                        peliculas.Add(pelicula);
                    }
                }

                ViewBag.PeliculaId = new SelectList(peliculas.OrderBy(p => p.Titulo).ToList(), "Id", "Titulo");
            }
            else
            {
                ViewBag.PeliculaId = new SelectList(db.Peliculas.OrderBy(p => p.Titulo).ToList(), "Id", "Titulo");
            }
            ViewBag.SalaId = id;
            return View("Create");
        }

        // POST: Funciones/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AdminValidator]
        public ActionResult Create([Bind(Include = "Id,PeliculaId,SalaId")] Funcion funcion)
        {
            if (ModelState.IsValid)
            {
                if (ObjectContext.GetObjectType(db.Salas.Find(funcion.SalaId).GetType()) == typeof(SalaHd) &&
                    ObjectContext.GetObjectType(db.Peliculas.Find(funcion.PeliculaId).GetType()) == typeof(Documental))
                    return RedirectToAction("Index", "Cines");

                if (db.Funciones.Where(f => f.SalaId == funcion.SalaId).Count() > 0)
                {
                    Funcion funcionAux = db.Funciones.Where(f => f.SalaId == funcion.SalaId).OrderByDescending(f => f.Id).First();
                    funcion.Horario = funcionAux.Horario.AddMinutes(funcionAux.Pelicula.Duracion + 15);

                    if (funcion.Horario.TimeOfDay > DateTime.Parse("02:00").TimeOfDay && funcion.Horario.TimeOfDay < DateTime.Parse("10:00").TimeOfDay) {
                        DateTime hora = DateTime.Parse("10:00");
                        funcion.Horario = funcion.Horario.Date.Add(hora.TimeOfDay);
                    }
                }
                else
                {
                    funcion.Horario = DateTime.Parse("10:00");
                }
                db.Funciones.Add(funcion);
                db.SaveChanges();
                return RedirectToAction("Index", "Cines");
            }
        

            ViewBag.PeliculaId = new SelectList(db.Peliculas, "Id", "Titulo", funcion.PeliculaId);
            ViewBag.SalaId = new SelectList(db.Salas, "Id", "Id", funcion.SalaId);
            return View(funcion);
        }

        // POST: Funciones/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AdminValidator]
        public ActionResult Delete(int id)
        {
            Sala sala = db.Salas.Find(id);

            foreach (Funcion funcion in sala.Funciones.ToList())
            {
                db.Funciones.Remove(funcion);
            }
            db.SaveChanges();
            return RedirectToAction("Index", "Cines");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
