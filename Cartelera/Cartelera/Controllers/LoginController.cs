﻿using Cartelera.DAL;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Cartelera.Controllers
{
    public class LoginController : Controller
    {

        private CarteleraContext db = new CarteleraContext();

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string User, int? Password)
        {
            if (User != null && Password.HasValue) {
                try
                {
                    if (db.Admins.Where(a => a.User == (string)User && a.Password == Password).First() != null)
                    {
                        Session["admin"] = "codificado";
                        return RedirectToAction("Index", "Peliculas");
                    }
                }
                catch (Exception)
                {
                    TempData["resultado"] = -1;
                    return View("Index");
                }
            }
            return View("Index");
        }

        public ActionResult Logout()
        {
            Session.Clear();
            return RedirectToAction("Index", "Peliculas");
        }
    }
}