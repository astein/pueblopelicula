﻿using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web.Mvc;
using Cartelera.DAL;
using Cartelera.Models;
using Cartelera.Models.Peliculas;
using Microsoft.Ajax.Utilities;

namespace Cartelera.Controllers
{
    public class PeliculasController : Controller
    {
        private CarteleraContext db = new CarteleraContext();
        
        // GET: Peliculas
        public ActionResult Index(int? cineId, string str)
        {
                //Combo
                ViewBag.Cines = new SelectList(db.Cines, "Id", "Nombre");


                if (cineId.HasValue && str.IsNullOrWhiteSpace())
                {
                    return View(db.Cines.Find(cineId).GetPeliculas().OrderBy(p => p.Titulo).ToList());
                }

                if (cineId.HasValue && !str.IsNullOrWhiteSpace())
                {
                    str = str.ToLower();
                    return View(db.Cines.Find(cineId).GetPeliculas().Where(p => p.Titulo.ToLower().Contains(str)
                                                        || p is Documental && (p as Documental).Tema.Descripcion.ToLower().Contains(str)
                                                        || p is Comercial && (p as Comercial).Actores.ToLower().Contains(str)).OrderBy(p => p.Titulo).ToList());
                
                }

                if (!str.IsNullOrWhiteSpace())
                {
                    str = str.ToLower();
                    return View(db.Peliculas.Where(p => p.Titulo.ToLower().Contains(str) 
                                                            || p is Documental && (p as Documental).Tema.Descripcion.ToLower().Contains(str) 
                                                            || p is Comercial && (p as Comercial).Actores.ToLower().Contains(str)).OrderBy(p => p.Titulo).ToList());
                }
                return View(db.Peliculas.OrderBy(p => p.Titulo).ToList());

        }
    }
}
