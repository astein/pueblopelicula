﻿using Cartelera.Models;
using Cartelera.Models.Peliculas;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Cartelera.DAL
{
    public class CarteleraContext : DbContext
    {

        public CarteleraContext() : base("CarteleraContext")
        {
            //Configuration.ProxyCreationEnabled = false;
        }

        public DbSet<Sala> Salas { get; set; }
        public DbSet<Pelicula> Peliculas { get; set; }
        public DbSet<Cine> Cines { get; set; }
        public DbSet<Funcion> Funciones { get; set; }
        public DbSet<Documental> Documentales { get; set; }
        public DbSet<Comercial> Comerciales { get; set; }
        public DbSet<Admin> Admins { get; set; }
        public DbSet<Tema> Temas { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}