﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Cartelera.Filters
{
    public class AdminValidator : ActionFilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if ((string)filterContext.HttpContext.Session["admin"] != "codificado")
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary { { "Controller", "Peliculas" }, { "Action", "Index" } });
        }
    }
}