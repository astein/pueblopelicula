using System.Collections.Generic;
using Cartelera.Models;
using Cartelera.Models.Peliculas;
using Cartelera.Models.Salas;

namespace Cartelera.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Cartelera.DAL.CarteleraContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Cartelera.DAL.CarteleraContext context)
        {
            var cines = new List<Cine>
            {
                new Cine{Nombre="Gerli",Salas = {new SalaHd(),new SalaHd(),new SalaHd(),new SalaNormal(),new SalaNormal()}},
                new Cine{Nombre="Bajo Flore",Salas = {new SalaHd(),new SalaHd(),new SalaHd(),new SalaNormal(),new SalaNormal()}},
                new Cine{Nombre="Ituzaing�",Salas = {new SalaHd(),new SalaHd(),new SalaHd(),new SalaNormal(),new SalaNormal()}},
                new Cine{Nombre="Jose C. Pa",Salas = {new SalaHd(),new SalaHd(),new SalaHd(),new SalaNormal(),new SalaNormal()}},
                new Cine{Nombre="Boquita",Salas = {new SalaHd(),new SalaHd(),new SalaHd(),new SalaNormal(),new SalaNormal()}},
            };
            cines.ForEach(c => context.Cines.Add(c));

            var temas = new List<Tema>
            {
                new Tema {Descripcion = "Biogr�fico"},
                new Tema {Descripcion = "Vida salvaje"},
                new Tema {Descripcion = "Ecolog�a"},
                new Tema {Descripcion = "Hist�rico"},
                new Tema {Descripcion = "Sociales"},
            };
            temas.ForEach(t => context.Temas.Add(t));

            var foto = "iVBORw0KGgoAAAANSUhEUgAAANcAAAE/BAMAAAAnM0vdAAAAA3NCSVQICAjb4U/gAAAAG1BMVEXn5+fj4+Pf39/X19fS0tLOzs7KysrGxsbCwsKAlBBbAAAACXBIWXMAAAsSAAALEgHS3X78AAAAFXRFWHRDcmVhdGlvbiBUaW1lADQvMTkvMTHDVGG0AAAAHHRFWHRTb2Z0d2FyZQBBZG9iZSBGaXJld29ya3MgQ1M0BrLToAAAAVlJREFUeJzt1rFKw1AUgOFUrTp20dnN1UF9AYuuFhEcK0pmFSFPIPjYEkouBSHxemjatN833eHATyC5OUUBAAAAAAAAAADkuZzmuQi09jNb05tA7CA3NhVbd2z8mEtsE2IPk6IYXXU00lAwNqsPJx2xNBSM3TeHNmlITGyVsV5f/V4/6jxiYmIbH7MWDO8XIza8mLVgQUxMTGxtMQtP+H92WJblvDm0SUNiOxd7rg+nHbE0FIy91N/rdUcsDQVjecTExMSWY71eV71exPPm0CYNiYmtMmYHWRATExNbjr3WN9FtRyMNBWN2ELEdjfX26h9VVfV5VhR7d1W7NBSM5RETExNbjvV6XX3Uh/OOWBoKxt6aQ5s0JCa2ytifXv33ZigYs4OIiYmJ/Y5ZeIb38xQbXszCsyC2U7Hj71zbG5v8PzbObX0Fnmz0lBmbBWIAAAAAAAAAAMBW+QHaf3bNxFjsPwAAAABJRU5ErkJggg==";
            var peliculas = new List<Pelicula>
            {
                new Comercial{Titulo="Juego de toldos",Duracion = 190,Actores = "Juan Lavas",Foto=foto},
                new Comercial{Titulo="Ba�eros 4",Duracion = 120,Actores = "Sol Perez",Foto=foto},
                new Comercial{Titulo="La de Adam Sandler",Duracion = 120,Actores = "La jeni aniston",Foto=foto},
                new Comercial{Titulo="Tony Stark",Duracion = 190,Actores = "RDJ",Foto=foto},
                new Comercial{Titulo="Jonathan Wickam",Duracion = 160,Actores = "Keanu",Foto=foto},
                new Documental{Titulo="El principito",Duracion = 100,Tema=temas.ElementAt(0),Foto=foto},
                new Documental{Titulo="Leones de Leon",Duracion = 120,Tema=temas.ElementAt(1),Foto=foto},
                new Documental{Titulo="�rboles imperiales",Duracion = 160,Tema=temas.ElementAt(2),Foto=foto},
                new Documental{Titulo="Hace 2000 a�os",Duracion = 80,Tema=temas.ElementAt(3),Foto=foto},
                new Documental{Titulo="Marx vs. Freud",Duracion = 100,Tema=temas.ElementAt(4),Foto=foto},
            };
            peliculas.ForEach(p => context.Peliculas.Add(p));

            var admin = new Admin() {User = "Admin", Password = 123};
            context.Admins.Add(admin);

            context.SaveChanges();
        }
    }
}
