﻿namespace Cartelera.Models
{
    public class Admin
    {
        public int Id { get; set; }
        public string User { get; set; }
        public int Password { get; set; }
    }
}