﻿using System.Collections.Generic;
using System.Linq;

namespace Cartelera.Models
{
    public class Cine
    {
        public int Id { get; set; }
        public string Nombre { get; set; }

        public virtual ICollection<Sala> Salas { get; set; } = new List<Sala>();


        public Sala GetSala(int id)
        {
            return Salas.Where(s => s.Id == id).First();
        }
                
        public List<Pelicula> GetPeliculas()
        {
            var peliculas = new List<Pelicula>();
            foreach (Sala sala in Salas.ToList())
            {
                foreach (Funcion funcion in sala.Funciones.ToList())
                {
                    if (!peliculas.Contains(funcion.Pelicula))
                        peliculas.Add(funcion.Pelicula);
                }
            }
            return peliculas.ToList();
        }

        public bool Contiene(Pelicula p)
        {
            foreach (Sala sala in Salas)
            {
                if(sala.Contiene(p))
                    return true;
            }
            return false;
        }
    }
}