﻿using System;

namespace Cartelera.Models
{
    public class Funcion
    {
        public int Id { get; set; }
        public DateTime Horario { get; set; }

        public int PeliculaId { get; set; }
        public virtual Pelicula Pelicula { get; set; }

        public virtual int SalaId { get; set; }
        public virtual Sala Sala { get; set; }

        public bool Contiene(Pelicula p)
        {
            if (PeliculaId == p.Id)
                return true;
            return false;
        }
    }
}