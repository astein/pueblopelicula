﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;

namespace Cartelera.Models.Peliculas
{
    public class Comercial : Pelicula
    {
        [Required]
        public string Actores { get; set; }
    }
}