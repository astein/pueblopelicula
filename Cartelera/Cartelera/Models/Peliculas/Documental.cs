﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;

namespace Cartelera.Models.Peliculas
{
    public class Documental : Pelicula
    {
        [Required]
        public int TemaId { get; set; }
        
        public virtual Tema Tema { get; set; }
    }
}