﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;

namespace Cartelera.Models
{
    public abstract class Pelicula
    {
        public int Id { get; set; }

        [Required]
        public string Titulo { get; set; }

        [Required]
        [Range(1, 360)]
        public int Duracion { get; set; }

        [Required]
        public string Foto { get; set; } //TODO Permitir subir la foto

        [NotMapped]
        public HttpPostedFileBase Archivo { get; set; }

        public string Sinopsis { get; set; }
        
    }
}