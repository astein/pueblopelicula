﻿using System.Collections.Generic;

namespace Cartelera.Models
{
    public abstract class Sala
    {
        public int Id { get; set; }
        public virtual ICollection<Funcion> Funciones { get; set; }
        
        public virtual int CineId { get; set; }
        public virtual Cine Cine { get; set; }
        
        public bool Contiene(Pelicula p)
        {
            foreach (var funcion in Funciones)
            {
                if (funcion.Contiene(p))
                    return true;
            }
            return false;
        }
    }
}