﻿/*function searchJs() {
    var input = document.getElementById("Search");
    var filter = input.value.toLowerCase();
    var nodes = document.getElementsByClassName("target");

    for (i = 0; i < nodes.length; i++) {
        if (nodes[i].innerText.toLowerCase().includes(filter)) {
            nodes[i].parentElement.style.display = "block";
        } else {
            nodes[i].parentElement.style.display = "none";
        }
    }
}*/


$("#Cines").change(function(event) {
    $('#Funciones').empty();
    if (event.target.value !== "")
        $.get("/Cines/GetSalasByCine/" + event.target.value,
            function(response) {
                var salas = JSON.parse(response);
                var str = "<select class='form-control' id='Salas' name='Salas'>";
                str += "<option value=''></option>";
                for (var i = 0; i < salas.length; i++) 
                {
                    str += "<option value=" + salas[i].Id + ">Sala " + salas[i].Id + " </option>";
                }
                str += "</select>";

                $("#Salas").html(str);
                });
});

$("#Salas").change(function (event) {
    if (event.target.value !== "") {
        var str;
        $('#Funciones').empty();
        $.get("/Cines/GetFuncionesBySala/" + event.target.value, function (response) {
            var funciones = JSON.parse(response);
            for (var i = 0; i < funciones.length; i++){
                str += "<tr>";
                str += "<td>" + funciones[i].Horario + " - <b>" + funciones[i].Pelicula.Titulo + "</b></td>";
                str += "</tr>";
            }
            $("#Funciones").append(str);
        

            $('#AgregarFuncion').empty();
            str = "<input style='margin-left: 5px' class='btn btn-primary' type='submit' name='agregar' value='Agregar funcion' />"
                +
                "<input id='Id' name='Id' type='hidden' value=" + event.target.value + ">";
            $("#AgregarFuncion").html(str);
            

            $('#BorrarFunciones').empty();
            str = "<input style='margin-left: 5px' class='btn btn-danger' type='submit' name='borrar' value='Borrar funciones' />"
                +
                "<input id='Id' name='Id' type='hidden' value=" + event.target.value + ">";
            $("#BorrarFunciones").html(str);
        });
    }
});